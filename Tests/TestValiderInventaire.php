<?php

require_once "./ValiderInventaire.classe.php";

use PHPUnit\Framework\TestCase;

class TestValiderInventaire extends TestCase
{
    private $selection;

    public function testQuantiteTotale()
    {
        $this->selection = new ValiderInventaire();
        $this->assertEquals(7,  $this->selection->selectNombreProduit());
    }
    public function testQuantiteInventaireZero()
    {
        $this->selection = new ValiderInventaire();
        $this->assertEquals(2,  $this->selection->selectNombreProduitQuantiteZero());
    }
}

