<?php

function loader($class)
{
//    require $class.'.php';
    $file = $class . '.php';
    if (file_exists($file)) {
        require $file;
    }
}

spl_autoload_register('loader');
