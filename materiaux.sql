
 create database materiaux;
 use materiaux;
 CREATE TABLE materiaux (
  numeroProduit int(10) NOT NULL primary key,
  nom varchar(100) NOT NULL,
  quantite integer(10) NOT NULL,
  seuil integer(10) NOT NULL
);

--
-- Contenu de la table `utilisateur`
--

INSERT INTO materiaux (numeroProduit, nom, quantite, seuil) VALUES
(10001, '2x4', '0', '500');
INSERT INTO materiaux (numeroProduit, nom, quantite, seuil) VALUES
(10002, '2x3', '0', '500');
INSERT INTO materiaux (numeroProduit, nom, quantite, seuil) VALUES
(10003, '2x6', '500', '500');
INSERT INTO materiaux (numeroProduit, nom, quantite, seuil) VALUES
(10004, '2x8', '500', '500');
INSERT INTO materiaux (numeroProduit, nom, quantite, seuil) VALUES
(10005, '2x10', '500', '500');
INSERT INTO materiaux (numeroProduit, nom, quantite, seuil) VALUES
(10006, '1x3', '500', '500');
INSERT INTO materiaux (numeroProduit, nom, quantite, seuil) VALUES
(10007, '1x6', '500', '500');

CREATE USER 'php'@'localhost' IDENTIFIED BY 'password';
GRANT SELECT ON materiaux.materiaux to 'php'@'localhost';
FLUSH PRIVILEGES;