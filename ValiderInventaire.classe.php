<?php
require_once './Connexion.classe.php';

class ValiderInventaire {
	public function selectNombreProduit(){
		$monPDO = new Connexion();
		$connexion = $monPDO->getPDO();
		$monStatement = $connexion->prepare("select count(*) from materiaux");
		$monStatement->execute();
		$liste = $monStatement->fetch()[0];

		return $liste;
	}
	public function selectNombreProduitQuantiteZero(){
		$monPDO = new Connexion();
		$connexion = $monPDO->getPDO();
		$monStatement = $connexion->prepare("select count(*) from materiaux where quantite=0");
		$monStatement->execute();
		$liste = $monStatement->fetch()[0];

		return $liste;
	}
}

